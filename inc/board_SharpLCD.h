/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.org/license.html
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_dma.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_rtc.h"
#include "em_usart.h"
#include "SQ013B7DH03.h"

static GFXINLINE void init_board(GDisplay *g) {
	(void) g;
	/* Init code was taken from the display driver from SiLabs. */
	// Choose the ULFRCO for the LFA clock domain (RTC)
	CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_ULFRCO);

	// Enable low frequency peripherals (RTC and DMA)
	CMU_ClockEnable(cmuClock_HFLE, true);
	CMU_ClockEnable(cmuClock_RTC, true);
	CMU_ClockEnable(cmuClock_DMA, true);

	// Enable high frequency peripherals (GPIO and USART)
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(PAL_SPI_USART_CLOCK, true);

	/* Setup RTC channel 0 for toggling the EXTCOMIN pin and the display. */
	// Initialize RTC
	RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;
	rtcInit.comp0Top = true;
	rtcInit.debugRun = false;
	rtcInit.enable   = true;
	RTC_Init(&rtcInit);
	const uint32_t rtcFreq = CMU_ClockFreqGet(cmuClock_RTC);

	// Setup RTC channels for toggling the display and for polarity inversion
	RTC_CompareSet(0, rtcFreq / POLARITY_INVERSION_FREQ);

	// Enable RTC Interrupt for toggling the display
	RTC_IntEnable(RTC_IEN_COMP0);
	NVIC_EnableIRQ(RTC_IRQn);

	// Enable GPIO pin for EXTCOMIN
	GPIO_PinModeSet((GPIO_Port_TypeDef)LCD_PORT_EXTCOMIN, LCD_PIN_EXTCOMIN, gpioModePushPull, 1);

  /* Configure SPI. */
  // Configure USART for synchronous mode
  USART_InitSync_TypeDef initSync = USART_INITSYNC_DEFAULT;
  initSync.baudrate = PAL_SPI_BAUDRATE;
  initSync.msbf = false; // Data is LSB first
  USART_InitSync(PAL_SPI_USART_UNIT, &initSync);

  // Route USART clock to display clock and USART TX to display SI
  PAL_SPI_USART_UNIT->ROUTE = PAL_SPI_USART_LOCATION
                              | USART_ROUTE_CLKPEN
                              | USART_ROUTE_TXPEN;

  // Set GPIO config to master for SPI communication
  GPIO_PinModeSet((GPIO_Port_TypeDef)LCD_PORT_DISP_SEL, LCD_PIN_DISP_SEL, gpioModePushPull, 1);
  GPIO_PinModeSet((GPIO_Port_TypeDef)LCD_PORT_SI, LCD_PIN_SI, gpioModePushPull, 0);
  GPIO_PinModeSet((GPIO_Port_TypeDef)LCD_PORT_SCLK, LCD_PIN_SCLK, gpioModePushPull, 0);
  GPIO_PinModeSet((GPIO_Port_TypeDef)LCD_PORT_SCS, LCD_PIN_SCS, gpioModePushPull, 0);

  // Send "CLEAR ALL" command to display over SPI
  GPIO_PinOutSet((GPIO_Port_TypeDef)LCD_PORT_SCS, LCD_PIN_SCS);
  USART_SpiTransfer(PAL_SPI_USART_UNIT, DISPLAY_COMMAND_CLEAR);
  GPIO_PinOutClear((GPIO_Port_TypeDef)LCD_PORT_SCS, LCD_PIN_SCS);
}

static GFXINLINE void post_init_board(GDisplay *g) {
	(void) g;
}

static GFXINLINE void setpin_reset(GDisplay *g, bool_t state) {
	(void) g;
	(void) state;
}

static GFXINLINE void set_backlight(GDisplay *g, uint8_t percent) {
	(void) g;
	(void) percent;
}

static GFXINLINE void acquire_bus(GDisplay *g) {
	(void) g;
	// Set SCS to high to begin SPI transfer
	GPIO_PinOutSet((GPIO_Port_TypeDef)LCD_PORT_SCS, LCD_PIN_SCS);
}

static GFXINLINE void release_bus(GDisplay *g) {
	(void) g;
	// Set SCS to low to end SPI transfer
	GPIO_PinOutClear((GPIO_Port_TypeDef)LCD_PORT_SCS, LCD_PIN_SCS);
}

static GFXINLINE void write_data(GDisplay *g, uint8_t data) {
  (void) g;
  PAL_SPI_USART_UNIT->CTRL &= ~USART_CTRL_MSBF;
  USART_SpiTransfer(PAL_SPI_USART_UNIT, data);
}

/***************************************************************************//**
 * @brief
 *   RTCC Interrupt handler, triggered on RTC channel 0 compare match. Toggles
 *   the EXTCOMIN pin 60 times per second, and transfers a frame 2 times per
 *   second.
 *
 *   This code also comes from the SiLabs display driver
 ******************************************************************************/
static volatile uint8_t extcominToggles = 0;
void RTC_IRQHandler(void)
{
  // Get and clear interrupt flags
  uint32_t pending = RTC_IntGet() & RTC_IF_COMP0;
  RTC_IntClear(pending);

  if (pending) {
    // Toggle the EXTCOMIN pin
    GPIO_PinOutToggle((GPIO_Port_TypeDef)LCD_PORT_EXTCOMIN, LCD_PIN_EXTCOMIN);
    ++extcominToggles;

    // Transfer a frame every half second
    if (extcominToggles >= (POLARITY_INVERSION_FREQ / DISPLAY_TOGGLE_FREQ)) {
      // Reset EXTCOMIN pin toggles
      extcominToggles = 0;
    }
  }
}

#endif /* _GDISP_LLD_BOARD_H */
