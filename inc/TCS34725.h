/**
* Copyright (c) 2018 Tijl Schepens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*/

/**
  ******************************************************************************
  * @file    inc/TCS34725.h
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    22-09-2017
  * @brief   TCS34725 header file.
  *
  *          This file contains all the TCS34725
  *          function prototypes and defines.
  ******************************************************************************
  */

#ifndef TCS34725_H
#define TCS34725_H

/** @addtogroup TCS34725
 * @{
 */

/* Needed to control the chip over I2C. */
#include "em_i2c.h"
/* Required for GPIO setup. */
#include "em_gpio.h"
/* Used to do clock setup. */
#include "em_cmu.h"
/* Required for basic functionality (sleep etc...) */
#include "gfx.h"

/** The I2C port being used. */
#define RGB_I2C_PORT      gpioPortD /**< GPIOD */
/** The GPIO pin which is used as SDA. */
#define RGB_I2C_SDA_PIN   6 /**< PD6 */
/** The GPIO pin which is used as SCL. */
#define RGB_I2C_SCL_PIN   7 /**< PD7 */

/** Write to the command register. */
#define TCS34725_CMD          (1 << 7)
/** Auto increment the register address with each read operation. */
#define TCS34725_AUTO_INC     (1 << 5)

/** The device address. */
#define TCS34725_ADDR         (0x29<<1)
/** Register to enable states and interrupts. */
#define TCS34725_REG_ENABLE   0x00
/** Color integration time (2.4ms - 614ms).
 *
 *  The value for this register is 2's complement and is
 *  calculated as follows:
 *  ATIME = 256 - Integration Time/2.4ms
 *  To read the integration time from the register:
 *  Integration Time = 2.4ms * (256 - ATIME)
 *  (see datasheet p8)
 */
#define TCS34725_REG_ATIME   0x01
/** Wait time register. */
#define TCS34725_REG_WTIME   0x03

/* Threshold values that specify when an interrupt is
 * triggered.
 */

/** Clear interrupt low threshold low byte. */
#define TCS34725_REG_AILTL    0x04
/** Clear interrupt low threshold high byte. */
#define TCS34725_REG_AILTH    0x05
/** Clear interrupt high threshold low byte. */
#define TCS34725_REG_AIHTL    0x06
/** Clear interrupt high threshold high byte. */
#define TCS34725_REG_AIHTH    0x07

/** Interrupt persistence filter.
 *
 *  This register specifies how many times a value has to go out-of-range
 *  to trigger an interrupt.
 */
#define TCS34725_REG_PERS     0x0C
/** Configuration register.
 *
 *  If WLONG = 0 the wait time is 2.4ms.
 *  If WLONG = 1 the wait time is 12*2.4ms = 28,8ms
 */
#define TCS34725_REG_CONFIG   0x0D
/** Control register. */
#define TCS34725_REG_CONTROL  0x0F
/** Device ID register. */
#define TCS34725_REG_ID       0x12
/** Device status register. */
#define TCS34725_REG_STATUS   0x13

/** Clear data low byte. */
#define TCS34725_REG_CDATAL   0x14
/** Clear data high byte. */
#define TCS43725_REG_CDATAH   0x15
/** Red data low byte. */
#define TCS34725_REG_RDATAL   0x16
/** Red data high byte. */
#define TCS34725_REG_RDATAH   0x17
/** Green data low byte. */
#define TCS34725_REG_GDATAL   0x18
/** Green data high byte. */
#define TCS34725_REG_GDATAH   0x19
/** Blue data low byte. */
#define TCS34725_REG_BDATAL   0x1A
/** Blue data high byte. */
#define TCS34725_REG_BDATAH   0x1B

/** Enum to indicate the status of the I2C transfer. */
typedef enum {
  Status_OK,          /**<  Everything was OK, transfer finished successfully. */
  Status_Error,       /**<  An error occurred, the transfer did not finish successfully. */
  Status_Timeout      /**<  The transfer was not completed within the specified timeout. */
} I2C_TransferStatus;

/** Used to give to functions to enable or disable a function. */
typedef enum {
  TCSEnable = 1,
  TCSDisable = 0
} TCS_EnableDisable;

/** Did the TCS module initialization fail or succeed? */
typedef enum {
  TCSInitSuccess = 1, /**< The initialization succeeded. */
  TCSInitFailed = -1  /**< The initialization failed. */
} TCS_InitStatus;

/* Public functions. */
void I2CInit(void);
I2C_TransferStatus I2CSendByte(uint8_t reg, uint8_t data);
I2C_TransferStatus I2CSendData(uint8_t reg, uint8_t *data, uint8_t len);
I2C_TransferStatus I2CReadByte(uint8_t reg, uint8_t *buf);
I2C_TransferStatus I2CReadData(uint8_t reg, uint8_t *buf, uint8_t len);
int8_t InitTCS();
I2C_TransferStatus TCS_Write_EnableReg(uint8_t IntEn, uint8_t WaitEn, uint8_t RGBCEn, uint8_t PON);
uint8_t TCS_Read_EnableReg();
I2C_TransferStatus TCS_Set_IntegrationTime(float IntegrationTime);
float TCS_Read_IntegrationTime();
I2C_TransferStatus TCS_Set_WaitTime(uint8_t Wtime);
uint8_t TCS_Get_WaitTime();
I2C_TransferStatus TCS_Write_ConfigReg(uint8_t Wlong);
I2C_TransferStatus TCS_Write_ControlReg(uint8_t gain);
uint8_t TCS_Read_Status();
uint16_t TCS_Read_ClearData();
uint16_t TCS_Read_RedData();
uint16_t TCS_Read_GreenData();
uint16_t TCS_Read_BlueData();

I2C_TransferStatus TCS_Enable();
I2C_TransferStatus TCS_Disable();

/** @}*/

#endif
