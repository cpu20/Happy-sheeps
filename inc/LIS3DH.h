/**
* Copyright (c) 2018 Tijl Schepens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*/

/**
  ******************************************************************************
  * @file    inc/LIS3DH.h
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    22-09-2017
  * @brief   LIS3DH header file.
  *
  *          This file contains all the LIS3DH
  *          function prototypes and defines.
  ******************************************************************************
  */

#ifndef LIS3DH_H
#define LIS3DH_H

/** @addtogroup LIS3DH
 * @{
 */

/* Used for SPI communication. */
#include "em_usart.h"
/* Required for GPIO setup. */
#include "em_gpio.h"
/* Used to do clock setup. */
#include "em_cmu.h"
/* Required for basic functionality (sleep etc...) */
#include "gfx.h"

/** The SPI port being used. */
#define LIS3DH_SPI_PORT       gpioPortE /**< GPIOE */
/** The GPIO pin which is used as MISO. */
#define LIS3DH_SPI_MISO_PIN   11  /**< PE11 */
/** The GPIO pin which is used as MOSI. */
#define LIS3DH_SPI_MOSI_PIN   10  /**< PE10 */
/** The GPIO pin which is used as SCK. */
#define LIS3DH_SPI_SCK_PIN    12  /**< PE12 */
/** The GPIO pin which is used as CS. */
#define LIS3DH_SPI_CS_PIN     13  /**< PE13 */
/** The MCU port used for the INT pin. */
#define LIS3DH_INT_PORT       gpioPortC /**< GPIOC */
/** The MCU pin used for the INT pin. */
#define LIS3DH_INT_PIN        0   /**< PC0 */

/* Bits used for the SPI transfer. */
/** When reading from the chip, the READ bits must be set in the first byte. */
#define LIS3DH_SPI_READ       (1 << 7)
/** To make the chip automatically increment the register address for consecutive reads/writes. */
#define LIS3DH_SPI_INCREMENT  (1 << 6)

/* LIS3DH registers. */

/** Register indicates the status of the data.
 *
 *  It indicates if a data overrun has occurred,
 *  and if new data is available.
 */
#define LIS3DH_REG_STATUS_REG_AUX       0x07

/** Conversion value of the 10-bit ADC channel 1 low byte. */
#define LIS3DH_REG_OUT_ADC1_L           0x08
/** Conversion value of the 10-bit ADC channel 1 high byte. */
#define LIS3DH_REG_OUT_ADC1_H           0x09
/** Conversion value of the 10-bit ADC channel 2 low byte. */
#define LIS3DH_REG_OUT_ADC2_L           0x0A
/** Conversion value of the 10-bit ADC channel 2 high byte. */
#define LIS3DH_REG_OUT_ADC2_H           0x0B
/** Conversion value of the 10-bit ADC channel 3 low byte. */
#define LIS3DH_REG_OUT_ADC3_L           0x0C
/** Conversion value of the 10-bit ADC channel 3 high byte. */
#define LIS3DH_REG_OUT_ADC3_H           0x0D

/** Device identification register. */
#define LIS3DH_REG_WHO_AM_I             0x0F
/** Control register 0 determines pull-up connection of the SDO/SA0 pin.
 *
 *  This register determines if the pull-up resistor for
 *  the SDO/SA0 pin is connected or disconnected.
 *  0: pull-up connected
 *  1: pull-up disconnected
 *
 *  Write to the register: 0x10 with the 7th bit the pull-up setting
 */
#define LIS3DH_REG_CTRL_REG0            0x1E
/** The configuration of the ADC and the temperature sensor.
 *  ADC_EN (bit7):
 *  1: ADC enabled
 *  0: ADC disabled
 *  TEMP_EN (bit6):
 *  1: temperature sensor enabled
 *  0: temperature sensor disabled
 */
#define LIS3DH_REG_TEMP_CFG_REG         0x1F
/** Control register 1. */
#define LIS3DH_REG_CTRL_REG1            0x20
/** Control register 2. */
#define LIS3DH_REG_CTRL_REG2            0x21
/** Control register 3. */
#define LIS3DH_REG_CTRL_REG3            0x22
/** Control register 4. */
#define LIS3DH_REG_CTRL_REG4            0x23
/** Control register 5. */
#define LIS3DH_REG_CTRL_REG5            0x24
/** Control register 6. */
#define LIS3DH_REG_CTRL_REG6            0x25
/** Set the reference value for the interrupt generation. */
#define LIS3DH_REG_REFERENCE            0x26
/** Register indicates the status of the data.
 *
 *  It indicates if a data overrun has occurred,
 *  and if new data is available.
 */
#define LIS3DH_REG_STATUS               0x27

/** X-axis acceleration data in 2's complement left-justified form low byte. */
#define LIS3DH_REG_OUT_X_L              0x28
/** X-axis acceleration data in 2's complement left-justified form high byte. */
#define LIS3DH_REG_OUT_X_H              0x29
/** Y-axis acceleration data in 2's complement left-justified form low byte. */
#define LIS3DH_REG_OUT_Y_L              0x2A
/** Y-axis acceleration data in 2's complement left-justified form high byte. */
#define LIS3DH_REG_OUT_Y_H              0x2B
/** Z-axis acceleration data in 2's complement left-justified form low byte. */
#define LIS3DH_REG_OUT_Z_L              0x2C
/** Z-axis acceleration data in 2's complement left-justified form high byte. */
#define LIS3DH_REG_OUT_Z_H              0x2D

/** FIFO control register. */
#define LIS3DH_REG_FIFO_CTRL_REG        0x2E
/** FIFO status register. */
#define LIS3DH_REG_FIFO_SRC_REG         0x2F

/** Interrupt 1 configuration register. */
#define LIS3DH_REG_INT1_CFG             0x30
/** Interrupt 1 source register. */
#define LIS3DH_REG_INT1_SRC             0x31
/** Interrupt 1 threshold register. */
#define LIS3DH_REG_INT1_THS             0x32
/** Interrupt 1 duration value. */
#define LIS3DH_REG_INT1_DURATION        0x33
/** Interrupt 2 configuration register. */
#define LIS3DH_REG_INT2_CFG             0x34
/** Interrupt 2 source register. */
#define LIS3DH_REG_INT2_SRC             0x35
/** Interrupt 2 threshold register. */
#define LIS3DH_REG_INT2_THS             0x36
/** Interrupt 2 duration value. */
#define LIS3DH_REG_INT2_DURATION        0x37

/** Click configuration register. */
#define LIS3DH_REG_CLICK_CFG            0x38
/** Click source register. */
#define LIS3DH_REG_CLICK_SRC            0x39
/** Click threshold register. */
#define LIS3DH_REG_CLICK_THS            0x3A
/** Click time limit value. */
#define LIS3DH_REG_TIME_LIMIT           0x3B
/** Click time latency value. */
#define LIS3DH_REG_TIME_LATENCY         0x3C
/** Click time window value. */
#define LIS3DH_REG_TIME_WINDOW          0x3D

/** Sleep-to-wake, return-to-sleep activation threshold in low-power. */
#define LIS3DH_REG_ACT_THS              0x3E
/** Sleep-to-wake, return-to-sleep duration. */
#define LIS3DH_REG_ACT_DUR              0x3F

/** Used to give to functions to enable or disable a function. */
typedef enum {
  LISEnable = 1,
  LISDisable = 0
} LIS_EnableDisable;

/** Options for the high-pass filter modes. */
typedef enum{
  LISHighPassNormal = 0,      /**< Normal high pass filter operation mode. */
  LISHighPassRefSignal = 1,   /**< Use a reference signal for filtering. */
  LISHighPassNormal2 = 2,     /**< Normal high pass filter operation mode. */
  LISHighPassAutoReset = 3    /**< Automatically reset the high-pass filter on an interrupt event. */
} LIS_HighPassModes;

/** Possible data rate configurations. */
typedef enum{
  LISDataRatePowerDown = 0,   /**< Power-down mode. */
  LISDataRate1Hz = 1,         /**< HR/Normal/LP mode 1Hz data rate. */
  LISDataRate10Hz = 2,        /**< HR/Normal/LP mode 10Hz data rate. */
  LISDataRate25Hz = 3,        /**< HR/Normal/LP mode 25Hz data rate. */
  LISDataRate50Hz = 4,        /**< HR/Normal/LP mode 50Hz data rate. */
  LISDataRate100Hz = 5,       /**< HR/Normal/LP mode 100Hz data rate. */
  LISDataRate200Hz = 6,       /**< HR/Normal/LP mode 200Hz data rate. */
  LISDataRate400Hz = 7,       /**< HR/Normal/LP mode 400Hz data rate. */
  LISDataRate1600Hz = 8,      /**< Low-power mode 1600Hz data rate. */
  LISDataRateHighest = 9      /**< HR/Normal mode 1334Hz data rate
                                   Low-power mode 5376Hz data rate. */
} LIS_DataRates;

/** Used for the selection of the full-scale configuration. */
typedef enum{
  LISFullScale2g = 0,         /**< +/-2g */
  LISFullScale4g = 1,         /**< +/-4g */
  LISFullScale8g = 2,         /**< +/-8g */
  LISFullScale16g = 3         /**< +/-16g */
} LIS_FullScaleSelect;

/** Configure the interrupt sources to be AND/ORed. */
typedef enum{
  LISORInterrupt = 0,
  LISANDInterrupt = 1
} LIS_ANDORInterrupt;

/* Low level peripheral driver functions. */
void SPIInit(void);
void SPISendByte(uint8_t reg, uint8_t data);
void SPISendData(uint8_t reg, uint8_t *data, uint8_t len);
uint8_t SPIReadByte(uint8_t reg);
void SPIReadData(uint8_t reg, uint8_t *buf, uint8_t len);

/* High level control functions. */
void InitLIS3DH();
void LIS3DH_Enable_Detection4D();
uint8_t LIS3DH_ReadStatus();
int16_t LIS3DH_ReadX();
int16_t LIS3DH_ReadY();
int16_t LIS3DH_ReadZ();

/* Low level sensor driver functions. */
void LIS3DH_Write_CtrlReg0(uint8_t pullEnable);
void LIS3DH_WriteTempCfgReg(uint8_t AdcEn, uint8_t TempEn);
void LIS3DH_WriteCtrlReg1(uint8_t DataRate, uint8_t LPen, uint8_t Zen, uint8_t Yen, uint8_t Xen);
void LIS3DH_WriteCtrlReg2(uint8_t HighPassMode, uint8_t HighPassCO, uint8_t DataFilter, uint8_t HighPassEnClick,
    uint8_t HighPassEnInt2, uint8_t HighPassEnInt1);
void LIS3DH_WriteCtrlReg3(uint8_t ClickInt1, uint8_t IA1Int1, uint8_t IA2Int1, uint8_t ZYXDAInt1, uint8_t DAInt1,
    uint8_t WatermarkFIFO, uint8_t OverrunFIFO);
void LIS3DH_WriteCtrlReg4(uint8_t BlockDataUpdate, uint8_t BigLittleEndian, uint8_t FScale, uint8_t HighRes,
    uint8_t SelfTest, uint8_t SPIInterface);
void LIS3DH_WriteCtrlReg5(uint8_t Boot, uint8_t FIFOEn, uint8_t LatchInt1, uint8_t En4DInt1, uint8_t LatchInt2, uint8_t En4DInt2);
void LIS3DH_WriteCtrlReg6(uint8_t ClickInt2, uint8_t IA1Int2, uint8_t IA2Int2, uint8_t BootInt2, uint8_t ActInt2,
    uint8_t IntPolarity);
void LIS3DH_SetReference(uint8_t reference);
void LIS3DH_WriteInt1Cfg(uint8_t AndOrInterrupt, uint8_t Enable6D, uint8_t ZHIE,
    uint8_t ZLIE, uint8_t YHIE, uint8_t YLIE, uint8_t XHIE, uint8_t XLIE);
uint8_t LIS3DH_ReadInt1Src();
void LIS3DH_WriteInt1Threshold(uint8_t Threshold);
void LIS3DH_WriteInt1Duration(uint8_t Duration);

/** @}*/

#endif
