# Happy-sheeps

Code to use on the Happy Gecko from SiliconLabs. It drives two seonsors, the LIS3DH and the TCS34725.  
The display is driven by µGFX: www.ugfx.io

NOTE:
-----
The libraries used here are outdated. They now have their own repository where they get updates. See:  
**TCS34725:** https://github.com/cpu20/TCS34725-lib  
**LIS3DH:** https://github.com/cpu20/LIS3DH-lib

Contact:
--------
You can always contact me with questions: tijl.schepens@gmail.com
