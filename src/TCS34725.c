/**
* Copyright (c) 2018 Tijl Schepens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*/

/**
  ******************************************************************************
  * @file    src/TCS34725.c
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    23-11-2017
  * @brief   TCS34725 driver file.
  *
  *          This file contains all the functions
  *          to perform I2C-communication and to
  *          use and configure the TCS34725.
  ******************************************************************************
  */

/** @addtogroup TCS34725
 * @{
 */

#include "TCS34725.h"

/**
 * @brief Initialize the I2C0 peripheral.
 */
void I2CInit(void)
{
  I2C_Init_TypeDef i2cInit;
  /* Enable the clock for the I2C peripheral. */
  CMU_ClockEnable(cmuClock_I2C0, true);
  /* Enable the clock for the GPIO. */
  CMU_ClockEnable(cmuClock_GPIO, true);
  /* Enable the clock for all the high frequency peripherals. */
  CMU_ClockEnable(cmuClock_HFPER, true);
  /* Setup the pins as open-drian with pull-up resistor. */
  GPIO_PinModeSet(RGB_I2C_PORT, RGB_I2C_SDA_PIN, gpioModeWiredAndPullUp, 1);
  GPIO_PinModeSet(RGB_I2C_PORT, RGB_I2C_SCL_PIN, gpioModeWiredAndPullUp, 1);

  I2C_Reset(I2C0);
  /* SDAPEN is SDA pin enable and SCLPEN is SCL pin enable. */
  /* The output of the different peripherals can be mapped to different pins
   * on the chip. This is done by setting the location bits in the I2C0's ROUTE
   * register. Here the output is mapped to location "one".
   */
  I2C0->ROUTE = I2C_ROUTE_SDAPEN
      | I2C_ROUTE_SCLPEN
      | I2C_ROUTE_LOCATION_LOC1;

  /* I2C peripheral initialization. */
  i2cInit.enable = true;  // Enable the I2C peripheral
  i2cInit.master = true;  // Configure the I2C into master mode
  i2cInit.freq = I2C_FREQ_STANDARD_MAX;
  i2cInit.refFreq = 0;
  i2cInit.clhr = i2cClockHLRStandard;
  I2C_Init(I2C0, &i2cInit);
  I2C_Enable(I2C0, true);
}

/**
 * @brief Send one byte to the chip over the I2C0 peripheral.
 *
 * @param reg       The register to which the byte is written.
 * @param data      The data to be send to the register.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus I2CSendByte(uint8_t reg, uint8_t data)
{
  I2C_TransferSeq_TypeDef i2cTransfer;
  I2C_TransferReturn_TypeDef i2cStatus;
  uint8_t timeout = 0;
  /* Setup the command byte. */
  reg |= TCS34725_CMD;
  /* Initialize an I2C transfer. */
  i2cTransfer.addr = TCS34725_ADDR;
  i2cTransfer.flags = I2C_FLAG_WRITE_WRITE;
  i2cTransfer.buf[0].data = &reg;
  i2cTransfer.buf[0].len = 1;
  i2cTransfer.buf[1].data = &data;
  i2cTransfer.buf[1].len = 1;
  i2cStatus = I2C_TransferInit(I2C0, &i2cTransfer);
  /* Wait for the I2C transfer to end. */
  while(i2cStatus == i2cTransferInProgress && timeout < 100){
    timeout++;
    i2cStatus = I2C_Transfer(I2C0);
    gfxSleepMilliseconds(1);
  }
  /* Check if the timeout didn't expire. */
  if(timeout >= 100)
    return Status_Timeout;

  /* Check if the I2C transfer succeeded. */
  if(i2cStatus != i2cTransferDone)
    return Status_Error;

  return Status_OK;
}

/**
 * @brief Send data over the I2C0 peripheral.
 *
 * This function writes multiple bytes to the chip. The first
 * write is at the given register address and then the address is
 * automatically incremented for the consecutive write operations.
 *
 * @param reg       The register to which the data is written.
 * @param data      A pointer to the buffer holding the data to send to the register.
 * @param len       The length of the data to be send to the register.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus I2CSendData(uint8_t reg, uint8_t *data, uint8_t len)
{
  I2C_TransferSeq_TypeDef i2cTransfer;
  I2C_TransferReturn_TypeDef i2cStatus;
  uint8_t timeout = 0;
  /* Make sure the chip auto increments the register address. */
  reg |= (TCS34725_AUTO_INC | TCS34725_CMD);
  /* Initialize an I2C transfer. */
  i2cTransfer.addr = TCS34725_ADDR;
  i2cTransfer.flags = I2C_FLAG_WRITE_WRITE;
  i2cTransfer.buf[0].data = &reg;
  i2cTransfer.buf[0].len = 1;
  i2cTransfer.buf[1].data = data;
  i2cTransfer.buf[1].len = len;
  i2cStatus = I2C_TransferInit(I2C0, &i2cTransfer);
  /* Wait for the I2C transfer to end. */
  while(i2cStatus == i2cTransferInProgress && timeout < 100){
    timeout++;
    i2cStatus = I2C_Transfer(I2C0);
    gfxSleepMilliseconds(1);
  }
  /* Check if the timeout didn't expire. */
  if(timeout >= 100)
    return Status_Timeout;

  /* Check if the I2C transfer succeeded. */
  if(i2cStatus != i2cTransferDone)
    return Status_Error;

  return Status_OK;
}

/**
 * @brief Read one byte from the chip via the I2C0 peripheral.
 *
 * @param reg       The register to which the byte is read from.
 * @param buf       A pointer to a buffer to store the received byte.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus I2CReadByte(uint8_t reg, uint8_t *buf)
{
  I2C_TransferSeq_TypeDef i2cTransfer;
  I2C_TransferReturn_TypeDef i2cStatus;
  uint8_t timeout = 0;
  /* Setup the command byte. */
  reg |= TCS34725_CMD;
  /* Initialize an I2C transfer for the register address write. */
  i2cTransfer.addr = TCS34725_ADDR;
  i2cTransfer.flags = I2C_FLAG_WRITE;
  i2cTransfer.buf[0].data = &reg;
  i2cTransfer.buf[0].len = 1;
  /* Write the register to read from to the chip. */
  i2cStatus = I2C_TransferInit(I2C0, &i2cTransfer);
  /* Wait for the I2C transfer to end. */
  while(i2cStatus == i2cTransferInProgress && timeout < 100){
    timeout++;
    i2cStatus = I2C_Transfer(I2C0);
    gfxSleepMilliseconds(1);
  }
  /* Check if the timeout didn't expire. */
  if(timeout >= 100)
    return Status_Timeout;
  /* Check if the I2C transfer succeeded. */
  if(i2cStatus != i2cTransferDone)
    return Status_Error;

  /* Initialize the I2C read transfer. */
  i2cTransfer.flags = I2C_FLAG_READ;
  i2cTransfer.buf[0].data = buf;
  i2cTransfer.buf[0].len = 1;
  /* Now read out the byte from the chip. */
  i2cStatus = I2C_TransferInit(I2C0, &i2cTransfer);
  /* Wait for the I2C transfer to end. */
  while(i2cStatus == i2cTransferInProgress && timeout < 100){
    timeout++;
    i2cStatus = I2C_Transfer(I2C0);
    gfxSleepMilliseconds(1);
  }
  /* Check if the timeout didn't expire. */
  if(timeout >= 100)
    return Status_Timeout;
  /* Check if the I2C transfer succeeded. */
  if(i2cStatus != i2cTransferDone)
    return Status_Error;

  return Status_OK;
}

/**
 * @brief Read data via the I2C0 peripheral.
 *
 * This function reads out multiple bytes from the chip. The first
 * read is at the given register address and then the address is
 * automatically incremented for the consecutive read operations.
 *
 * @param reg       The register to which the data is read from.
 * @param buf       Pointer to a buffer to hold the received data.
 * @param len       The amount of data to read from the chip.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus I2CReadData(uint8_t reg, uint8_t *buf, uint8_t len)
{
  I2C_TransferSeq_TypeDef i2cTransfer;
  I2C_TransferReturn_TypeDef i2cStatus;
  uint8_t timeout = 0;
  /* Make sure the chip auto increments the register address. */
  reg |= (TCS34725_AUTO_INC | TCS34725_CMD);
  /* Initialize an I2C transfer for the register address write. */
  i2cTransfer.addr = TCS34725_ADDR;
  i2cTransfer.flags = I2C_FLAG_WRITE;
  i2cTransfer.buf[0].data = &reg;
  i2cTransfer.buf[0].len = 1;
  /* Write the register to read from to the chip. */
  i2cStatus = I2C_TransferInit(I2C0, &i2cTransfer);
  /* Wait for the I2C transfer to end. */
  while(i2cStatus == i2cTransferInProgress && timeout < 100){
    timeout++;
    i2cStatus = I2C_Transfer(I2C0);
    gfxSleepMilliseconds(1);
  }
  /* Check if the timeout didn't expire. */
  if(timeout >= 100)
    return Status_Timeout;
  /* Check if the I2C transfer succeeded. */
  if(i2cStatus != i2cTransferDone)
    return Status_Error;

  /* Initialize the I2C read transfer. */
  i2cTransfer.flags = I2C_FLAG_READ;
  i2cTransfer.buf[0].data = buf;
  i2cTransfer.buf[0].len = len;
  /* Now read out the desired bytes from the chip. */
  i2cStatus = I2C_TransferInit(I2C0, &i2cTransfer);
  /* Wait for the I2C transfer to end. */
  while(i2cStatus == i2cTransferInProgress && timeout < 100){
    timeout++;
    i2cStatus = I2C_Transfer(I2C0);
    gfxSleepMilliseconds(1);
  }
  /* Check if the timeout didn't expire. */
  if(timeout >= 100)
    return Status_Timeout;
  /* Check if the I2C transfer succeeded. */
  if(i2cStatus != i2cTransferDone)
    return Status_Error;

  return Status_OK;
}

/**
 * @brief Initialize the module with some basic values.
 *
 * @return TCSInitSuccess: Initialization was a success
 *         TCSInitFailed: Something went wrong with an I2C transfer.
 */
TCS_InitStatus InitTCS()
{
  /* First initialize the I2C peripheral. */
  I2CInit();
  /* Enable the chip and the RGBC two-channel ADC. Don't enable the interrupts. */
  /* Also enable the wait function to be able to save power. */
  uint8_t res = TCS_Write_EnableReg(TCSDisable, TCSEnable, TCSEnable, TCSEnable);
  /* Did the I2C transfer succeed? */
  if(res != i2cTransferDone)
    return -1;
  /* This sets a wait time of 100.8ms. */
  res = TCS_Set_WaitTime(42);
  /* Did the I2C transfer succeed? */
  if(res != i2cTransferDone)
    return TCSInitFailed;
  /* Wait for at least 2.4ms to make sure the system can settle and boot. */
  gfxSleepMilliseconds(3);
  /* End the initialization. */
  return TCSInitSuccess;
}

/**
 * @brief Enable the TCS Module
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 */
I2C_TransferStatus TCS_Enable()
{
  /* Enable the chip. */
  I2C_TransferStatus res = TCS_Write_EnableReg(TCSDisable, TCSEnable, TCSEnable, TCSEnable);
  /* Did the I2C transfer succeed? */
  if(res != Status_OK)
    return Status_Error;
  /* Wait for at least 2.4ms to make sure the system can settle and boot. */
  gfxSleepMilliseconds(3);
  /* Everything succeeded. */
  return Status_OK;
}

/**
 * @brief Disable the TCS Module
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 */
I2C_TransferStatus TCS_Disable()
{
  /* Disable the chip. */
  I2C_TransferStatus res = TCS_Write_EnableReg(TCSDisable, TCSEnable, TCSEnable, TCSDisable);
  /* Did the I2C transfer succeed? */
  if(res != Status_OK)
    return Status_Error;
  /* Everything succeeded. */
  return Status_OK;
}

/**
 * @brief Enable the desired components.
 *
 * @param IntEn     Enable RGBC interrupts.
 * @param WaitEn    This enables or disables the wait timer.
 * @param RGBCEn    Enable the RGBC measurements.
 * @param PON       Power ON the chip, this activates oscillator.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus TCS_Write_EnableReg(uint8_t IntEn, uint8_t WaitEn, uint8_t RGBCEn, uint8_t PON)
{
  uint8_t Data = (IntEn << 4) | (WaitEn << 3) | (RGBCEn << 1) | PON;
  return I2CSendByte(TCS34725_REG_ENABLE, Data);
}

/**
 * @brief Read out the enable register.
 *
 * @return  The value of the enable register.
 *          -1: If the transfer failed.
 */
uint8_t TCS_Read_EnableReg()
{
  uint8_t EnableReg;
  I2CReadByte(TCS34725_REG_ENABLE, &EnableReg);

  return EnableReg;
}

/**
 * @brief Control the RGBC integration time in steps of 2.4ms
 *
 * @param           IntegrationTime   The desired integration time in steps of 2.4ms.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus TCS_Set_IntegrationTime(float IntegrationTime)
{
  return I2CSendByte(TCS34725_REG_ATIME, (256 - (IntegrationTime/2.4)));
}

/**
 * @brief Read out the current set integration time for the RGBC values.
 *
 * @retval The integration time that is currently set in steps of 2.4ms.
 */
float TCS_Read_IntegrationTime()
{
  uint8_t IntegrationTime;
  I2CReadByte(TCS34725_REG_ATIME, &IntegrationTime);

  return 2.4 * (float)(256 - IntegrationTime);
}

/**
 * @brief Set the wait time multiplier between two RGBC measurements.
 *
 * @param Wtime     The number to multiply with the wait time.
 *                  The wait time depends on the WLONG bit.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus TCS_Set_WaitTime(uint8_t Wtime)
{
  /* Convert the number to two's complement. */
  Wtime = (Wtime ^ 0xFF) + 1;

  return I2CSendByte(TCS34725_REG_WTIME, Wtime);
}

/**
 * @brief Read out the current wait time multiplier.
 *
 * @return The currently set wait time multiplier.
 */
uint8_t TCS_Get_WaitTime()
{
  uint8_t Wtime;
  I2CReadByte(TCS34725_REG_WTIME, &Wtime);
  /* Convert the value from 2's complement. */
  return ((Wtime - 1) ^ 0xFF);
}

/**
 * @brief Set the wait time step.
 *
 * @param Wlong     Setting WLONG 0 means that the wait time step is 2.4ms.
 *                  Setting WLONG to 1 multiplies this by 12 and gives 28.8ms.
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus TCS_Write_ConfigReg(uint8_t Wlong)
{
  return I2CSendByte(TCS34725_REG_CONFIG, (Wlong << 1));
}

/**
 * @brief Set the gain for the analog block.
 *
 * @param gain      The gain for the analog block, possible values:
 *                  1x gain
 *                  4x gain
 *                  16x gain
 *                  60x gain
 *
 * @retval          Status_OK:      The transfer was a success.
 * @retval          Status_Error:   The transfer failed.
 * @retval          Status_Timeout: The transfer timed out.
 *
 * @sa I2C_TransferStatus
 */
I2C_TransferStatus TCS_Write_ControlReg(uint8_t gain)
{
  switch(gain){
  case 1:
    return I2CSendByte(TCS34725_REG_CONTROL, 0x00);
    break;
  case 4:
    return I2CSendByte(TCS34725_REG_CONTROL, 0x01);
    break;
  case 16:
    return I2CSendByte(TCS34725_REG_CONTROL, 0x02);
    break;
  case 60:
    return I2CSendByte(TCS34725_REG_CONTROL, 0x03);
    break;
  default:
    return Status_Error;
  }
}

/**
 * @brief Read out the status register.
 *
 * @retval The read out status.
 */
uint8_t TCS_Read_Status()
{
  uint8_t status;
  I2CReadByte(TCS34725_REG_STATUS, &status);

  return status;
}

/**
 * @brief Read out the clear data.
 *
 * @retval The clear data.
 */
uint16_t TCS_Read_ClearData()
{
  uint8_t clearData[2];
  /* Read out the clear data low and high byte at once to prevent problems. */
  I2CReadData(TCS34725_REG_CDATAL, clearData, 2);

  return (clearData[1] << 8) | clearData[0];
}

/**
 * @brief Read out the red data.
 *
 * @retval The red data.
 */
uint16_t TCS_Read_RedData()
{
  uint8_t redData[2];
  /* Read out the clear data low and high byte at once to prevent problems. */
  I2CReadData(TCS34725_REG_RDATAL, redData, 2);

  return (redData[1] << 8) | redData[0];
}

/**
 * @brief Read out the green data.
 *
 * @retval The green data.
 */
uint16_t TCS_Read_GreenData()
{
  uint8_t greenData[2];
  /* Read out the clear data low and high byte at once to prevent problems. */
  I2CReadData(TCS34725_REG_GDATAL, greenData, 2);

  return (greenData[1] << 8) | greenData[0];
}

/**
 * @brief Read out the blue data.
 *
 * @retval The blue data.
 */
uint16_t TCS_Read_BlueData()
{
  uint8_t blueData[2];
  /* Read out the clear data low and high byte at once to prevent problems. */
  I2CReadData(TCS34725_REG_BDATAL, blueData, 2);

  return (blueData[1] << 8) | blueData[0];
}

/** @}*/
