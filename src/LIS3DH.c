/**
* Copyright (c) 2018 Tijl Schepens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*/

/**
  ******************************************************************************
  * @file    src/LIS3DH.c
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    23-11-2017
  * @brief   LIS3DH driver file.
  *
  *          This file contains all the functions
  *          to perform SPI-communication and to
  *          use and configure the LIS3DH.
  ******************************************************************************
  */

/** @addtogroup LIS3DH
 * @{
 */

#include "LIS3DH.h"

/**
 * @brief Initialize the USART0 peripheral to operate as SPI master.
 */
void SPIInit(void)
{
  USART_InitSync_TypeDef spiInit;
  /* Enable the clock for the USART0 peripheral. */
  CMU_ClockEnable(cmuClock_USART0, true);
  /* Enable the clock for the GPIO. */
  CMU_ClockEnable(cmuClock_GPIO, true);
  /* Enable the clock for all the high frequency peripherals. */
  CMU_ClockEnable(cmuClock_HFPER, true);

  /* SPI peripheral initialization. */
  spiInit.enable = usartEnable; // Enable the peripheral after the initialization.
  spiInit.refFreq = 0; // Currently configured reference clock is used.
  spiInit.baudrate = 3500000; // 1Mbits/s default speed.
  spiInit.databits = usartDatabits8; // 8 data bits per transfer.
  spiInit.master = true; // The MCU is the SPI master.
  spiInit.msbf = true; // Send the most significant bit first.
  spiInit.clockMode = usartClockMode0; // Clock idle low, sample on rising edge.
  spiInit.prsRxEnable = false; // Disable Rx via PRS.
  spiInit.autoTx = false; // Auto transmit disabled.
  USART_InitSync(USART0, &spiInit);

  /* Setup the pins as push-pull outputs. */
  GPIO_PinModeSet(LIS3DH_SPI_PORT, LIS3DH_SPI_MISO_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(LIS3DH_SPI_PORT, LIS3DH_SPI_MOSI_PIN, gpioModePushPull, 0);
  GPIO_PinModeSet(LIS3DH_SPI_PORT, LIS3DH_SPI_SCK_PIN, gpioModePushPull, 0);
  GPIO_PinModeSet(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN, gpioModePushPull, 1);
  /* Reset the whole peripheral first. */
  //USART_Reset(USART0);

  /* CLKPEN is the clock pin enable, TXPEN is the MOSI pin enable and RXPEN is the MISO pin enable.
   * The output of the different peripherals can be mapped to different pins
   * on the chip. This is done by setting the location bits in the USART0's ROUTE
   * register. Here the output is mapped to location "zero".
   */
  USART0->ROUTE = USART_ROUTE_LOCATION_LOC0
                              | USART_ROUTE_RXPEN
                              | USART_ROUTE_CLKPEN
                              | USART_ROUTE_TXPEN;
}

/**
 * @brief Send one byte to the chip over the SPI peripheral.
 *
 * @param reg       The register to which the byte is written.
 * @param data      The data to be send to the register.
 */
void SPISendByte(uint8_t reg, uint8_t data)
{
  USART0->CTRL |= USART_CTRL_MSBF;
  /* Set the chip select low. */
  GPIO_PinOutClear(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
  /* Send the register to which to write the byte. */
  USART_SpiTransfer(USART0, reg);
  /* Send a byte to the chip. (this discards any data in the RXbuffer) */
  USART_SpiTransfer(USART0, data);
  /* Bring the chip select high again. */
  GPIO_PinOutSet(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
}

/**
 * @brief Send data over the SPI peripheral.
 *
 * This function writes multiple bytes to the chip. The first
 * write is at the given register address and then the address is
 * automatically incremented for the consecutive write operations.
 *
 * @param reg       The register to which the data is written.
 * @param data      A pointer to the buffer holding the data to send to the register.
 * @param len       The length of the data to be send to the register.
 */
void SPISendData(uint8_t reg, uint8_t *data, uint8_t len)
{
  USART0->CTRL |= USART_CTRL_MSBF;
  /* Set the chip select low. */
  GPIO_PinOutClear(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
  /* Send the register address to the chip to which the data will be written.
   * Also let the chip automatically increment the register address with every
   * write operation.
   */
  USART_SpiTransfer(USART0, reg | LIS3DH_SPI_INCREMENT);
  /* Send all the bytes to the chip. */
  for(int i=0;i<len;i++){
    /* Send a data byte to the chip. (this discards any data in the RXbuffer) */
    USART_SpiTransfer(USART0, data[i]);
  }
  /* Bring the chip select high again. */
  GPIO_PinOutSet(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
}

/**
 * @brief Read one byte from the chip via the SPI peripheral.
 *
 * @param reg       The register to which the byte is read from.
 * @param buf       A pointer to a buffer to store the received byte.
 *
 * @retval The byte that was read from the register.
 */
uint8_t SPIReadByte(uint8_t reg)
{
  USART0->CTRL |= USART_CTRL_MSBF;
  /* Set the chip select low. */
  GPIO_PinOutClear(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
  /* Send the register address and receive the data from the chip. */
  USART_SpiTransfer(USART0, reg | LIS3DH_SPI_READ);
  uint8_t regData = USART_SpiTransfer(USART0, 0x00);
  /* Bring the chip select high again. */
  GPIO_PinOutSet(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);

  return regData;
}

/**
 * @brief Read data via the SPI peripheral.
 *
 * This function reads out multiple bytes from the chip. The first
 * read is at the given register address and then the address is
 * automatically incremented for the consecutive read operations.
 *
 * @param reg       The register to which the data is read from.
 * @param buf       Pointer to a buffer to hold the received data.
 * @param len       The amount of data to read from the chip.
 */
void SPIReadData(uint8_t reg, uint8_t *buf, uint8_t len)
{
  USART0->CTRL |= USART_CTRL_MSBF;
  /* Set the chip select low. */
  GPIO_PinOutClear(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
  /* Send the register address to the chip from which the data will be read. */
  USART_SpiTransfer(USART0, reg | LIS3DH_SPI_INCREMENT | LIS3DH_SPI_READ);
  for(int i=0;i<len;i++){
    /* Send dummy data and read out all the bytes. */
    buf[i] = USART_SpiTransfer(USART0, 0x00);
  }
  GPIO_PinOutSet(LIS3DH_SPI_PORT, LIS3DH_SPI_CS_PIN);
}

/**
 * @brief Initialize the module with some basic values.
 */
void InitLIS3DH()
{
  /* First initialize the SPI peripheral. */
  SPIInit();
  /* Configure the interrupt pin. */
  GPIO_PinModeSet(LIS3DH_INT_PORT, LIS3DH_INT_PIN, gpioModeInput, 0);
  GPIO_ExtIntConfig(LIS3DH_INT_PORT, LIS3DH_INT_PIN, LIS3DH_INT_PIN,
        true, false, true);
  GPIO_IntClear(1<<LIS3DH_INT_PIN);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
  /* Select the desired data rate, put the device in low-power mode and enable all axis. */
  LIS3DH_WriteCtrlReg1(LISDataRate10Hz, LISEnable, LISEnable, LISEnable, LISEnable);
}

/**
 * @brief Enable 4D-detection to detect device rotations using interrupts.
 *
 * This function will enable the 4D-interrupt generation via INT1. It configures
 * the angle from which rotation is detected and also configures the duration
 * before the rotation is detected.
 */
void LIS3DH_Enable_Detection4D()
{
  /* Enable interrupt generation IA1 routed to pin INT1. */
  LIS3DH_WriteCtrlReg3(LISDisable, LISEnable, LISDisable, LISDisable, LISDisable,
      LISDisable, LISDisable);
  /* Enable register update when reading MSB and LSB. */
  LIS3DH_WriteCtrlReg4(1, 0, LISFullScale2g, LISDisable, LISDisable, 0);
  /* Enable the 4D-interrupt on INT1. Read the INT1_SRC register to reset the interrupt. */
  LIS3DH_WriteCtrlReg5(LISDisable, LISDisable, LISEnable, LISEnable, LISDisable, LISDisable);
  /* Arcsin(0.866)=60° -> The angle in which the x-axis must be to generate an interrupt.
   * This is reached by: 866mg/(16mg/LSB) = 54,13LSB -> 54LSB rounded.
   */
  LIS3DH_WriteInt1Threshold(54);
  /* For the duration we want 400ms. duration * ODR = INT1_THRESHOLD -> 400ms * 10Hz = 4 */
  LIS3DH_WriteInt1Duration(4);
  /* Enable 4D detection on INT1 by setting the 6D bit in the INT1_CFG. */
  LIS3DH_WriteInt1Cfg(LISORInterrupt, LISEnable, LISDisable, LISDisable, LISEnable,
      LISEnable, LISEnable, LISEnable);
}

/**
 * @brief Enable or disable SDO/SA0 pull-up resistor.
 *
 * @param pullEnable  Enable or disable the pull-up resistor
 *                    on the SDO/SA0 pin.
 *                    1: enable pull-up
 *                    0: disable pull-up
 */
void LIS3DH_Write_CtrlReg0(uint8_t pullEnable)
{
  SPISendByte(LIS3DH_REG_CTRL_REG0, ((pullEnable << 7) | 0x10));
}

/**
 * @brief Enable/disable the ADC and temperature sensor.
 *
 * @param AdcEn       Enable or disable the external ADC.
 * @param TempEn      Enable or disable the temperature sensor.
 */
void LIS3DH_WriteTempCfgReg(uint8_t AdcEn, uint8_t TempEn)
{
  SPISendByte(LIS3DH_REG_TEMP_CFG_REG, ((AdcEn << 7) | (TempEn << 6)));
}

/**
 * @brief Set configuration register 1.
 *
 * @param DataRate    Select a desired data rate at which the
 *                    accelerometer works. See @sa LIS_DataRates
 * @param LPen        Select Normal/high-resolution mode or low-power mode.
 *                    0: Normal/high-resolution mode
 *                    1: low-power mode
 * @param Zen         Enable the Z-axis
 * @param Yen         Enable the Y-axis
 * @param Xen         Enable the X-axis
 *
 * @sa LIS_DataRates
 */
void LIS3DH_WriteCtrlReg1(uint8_t DataRate, uint8_t LPen, uint8_t Zen, uint8_t Yen, uint8_t Xen)
{
  SPISendByte(LIS3DH_REG_CTRL_REG1, ((DataRate << 4) | (LPen << 3) | (Zen << 2) | (Yen << 1) | Xen));
}

/**
 * @brief Set configuration register 2.
 *
 * @param HighPassMode    Select the high-pass filter mode.
 *                        Modes are defined via @sa LIS_HighPassModes
 * @param HighPassCO      Select the cutoff frequency for the high-pass filter.
 * @param DataFilter      0: Internal filter is bypassed.
 *                        1: Data from the internal filter is sent to the output
 *                        register and the FIFO.
 * @param HighPassEnClick Enable or bypass the filter for the CLICK function.
 * @param HighPassEnInt2  Enable or bypass the filter for the AOI function on interrupt 2.
 * @param HighPassEnInt1  Enable or bypass the filter for the AOI function on interrupt 1.
 *
 * @sa LIS_HighPassModes
 */
void LIS3DH_WriteCtrlReg2(uint8_t HighPassMode, uint8_t HighPassCO, uint8_t DataFilter, uint8_t HighPassEnClick,
    uint8_t HighPassEnInt2, uint8_t HighPassEnInt1)
{
  SPISendByte(LIS3DH_REG_CTRL_REG2, ((HighPassMode << 6) | (HighPassCO << 4) | (DataFilter << 3) | (HighPassEnClick << 2)
      | (HighPassEnInt2 << 1) | HighPassEnInt1));
}

/**
 * @brief Enable or disable interrupts.
 *
 * @param ClickInt1       Enable or disable Click interrupt on INT1.
 * @param IA1Int1         Enable or disable IA1 interrupt on INT1.
 * @param IA2Int1         Enable or disable IA2 interrupt on INT1.
 * @param ZYXDAInt1       Enable or disable ZYXDA interrupt on INT1.
 * @param DAInt1          Enable or disable 321DA interrupt on INT1.
 * @param WatermarkFIFO   Enable or disable FIFO watermark interrupt on INT1.
 * @param OverrunFIFO     Enable or disable FIFO overrun interrupt on INT1.
 */
void LIS3DH_WriteCtrlReg3(uint8_t ClickInt1, uint8_t IA1Int1, uint8_t IA2Int1, uint8_t ZYXDAInt1, uint8_t DAInt1,
    uint8_t WatermarkFIFO, uint8_t OverrunFIFO)
{
  SPISendByte(LIS3DH_REG_CTRL_REG3, ((ClickInt1 << 7) | (IA1Int1 << 6) | (IA2Int1 << 5) | (ZYXDAInt1 << 4) |
      (DAInt1 << 3) | (WatermarkFIFO << 2) | (OverrunFIFO << 1)));
}

/**
 * @brief Set configuration register 4.
 *
 * @param BlockDataUpdate 0: Continuous update
 *                        1: output registers are not updated until MSB and LSB reading
 * @param BigLittleEndian 0: Data LSB @ lower address
 *                        1: Data MSB @ lower address (Only in high-resolution mode.)
 * @param FScale          Full-scale selection. See @sa LIS_FullScaleSelect
 * @param HighRes         Enable or disable high-resolution output mode.
 * @param SelfTest        Select a specific self-test. See datasheet for details.
 * @param SPIInterface    Select 4-wire (0) or 3-wire (1) interface.
 *
 * @sa LIS_FullScaleSelect
 */
void LIS3DH_WriteCtrlReg4(uint8_t BlockDataUpdate, uint8_t BigLittleEndian, uint8_t FScale, uint8_t HighRes,
    uint8_t SelfTest, uint8_t SPIInterface)
{
  SPISendByte(LIS3DH_REG_CTRL_REG4, ((BlockDataUpdate << 7) | (BigLittleEndian << 6) | (FScale << 4) | (HighRes << 3) |
      (SelfTest << 1) | SPIInterface));
}

/**
 * @brief Set configuration register 5.
 *
 * @param Boot            Reboot memory content or not.
 * @param FIFOEn          Enable or disable the FIFO buffer.
 * @param LatchInt1       Latch the interrupt request on INT1_SRC register.
 * @param En4DInt1        Enable the 4D detection on INT1.
 * @param LatchInt2       Latch the interrupt request on INT2_SRC register.
 * @param EN4DInt2        Enable the 4D detection on INT2.
 */
void LIS3DH_WriteCtrlReg5(uint8_t Boot, uint8_t FIFOEn, uint8_t LatchInt1, uint8_t En4DInt1, uint8_t LatchInt2, uint8_t En4DInt2)
{
  SPISendByte(LIS3DH_REG_CTRL_REG5, ((Boot << 7) | (FIFOEn << 6) | (LatchInt1 << 3) | (En4DInt1 << 2) | (LatchInt2 << 1) |
      En4DInt2));
}

/**
 * @brief Enable or disable interrupts.
 *
 * @param ClickInt2       Enable or disable Click interrupt on INT2.
 * @param IA1Int2         Enable or disable IA1 interrupt on INT2.
 * @param IA2Int2         Enable or disable IA2 interrupt on INT2.
 * @param BootInt2        Enable or disable Boot interrupt on INT2.
 * @param ActInt2         Enable or disable activity interrupt on INT2.
 * @param IntPolarity     Set the interrupt polarity. 1: active-low 0: active-high
 */
void LIS3DH_WriteCtrlReg6(uint8_t ClickInt2, uint8_t IA1Int2, uint8_t IA2Int2, uint8_t BootInt2, uint8_t ActInt2,
    uint8_t IntPolarity)
{
  SPISendByte(LIS3DH_REG_CTRL_REG3, ((ClickInt2 << 7) | (IA1Int2 << 6) | (IA2Int2 << 5) | (BootInt2 << 4) |
      (ActInt2 << 3) | (IntPolarity << 1)));
}

/**
 * @brief Set the reference value for interrupt generation.
 *
 * @param reference       The desired reference value for interrupt generation.
 */
void LIS3DH_SetReference(uint8_t reference)
{
  SPISendByte(LIS3DH_REG_REFERENCE, reference);
}

/**
 * @brief Read out the status register.
 *
 * @return The value from the status register.
 */
uint8_t LIS3DH_ReadStatus()
{
  return SPIReadByte(LIS3DH_REG_STATUS);
}

/**
 * @brief Read out the measured X-data.
 *
 * @return The read out X-data.
 */
int16_t LIS3DH_ReadX()
{
  uint8_t xData[2];
  SPIReadData(LIS3DH_REG_OUT_X_L, xData, 2);
  /* The data is signed two's complement. */
  return *(int16_t *)xData;
}

/**
 * @brief Read out the measured Y-data.
 *
 * @return The read out Y-data.
 */
int16_t LIS3DH_ReadY()
{
  uint8_t yData[2];
  SPIReadData(LIS3DH_REG_OUT_Y_L, yData, 2);

  return *(uint16_t *)yData;
}

/**
 * @brief Read out the measured Z-data.
 *
 * @return The read out Z-data.
 */
int16_t LIS3DH_ReadZ()
{
  uint8_t zData[2];
  SPIReadData(LIS3DH_REG_OUT_Z_L, zData, 2);

  return *(uint16_t *)zData;
}

/**
 * @brief Configure interrupt1 generation.
 *
 * @param AndOrInterrupt      Configure if an AND/OR operation of interrupt
 *                            events is performed. @sa LIS_ANDORInterrupt
 * @param Enable6D            Enable or disable the 6D detection function.
 *                            See: Table 55
 * @param ZHIE                Enable interrupt generation on Z high event
 *                            or on direction recognition.
 * @param ZLIE                Enable interrupt generation on Z low event
 *                            or on direction recognition.
 * @param YHIE                Enable interrupt generation on Y high event
 *                            or on direction recognition.
 * @param YLIE                Enable interrupt generation on Y low event
 *                            or on direction recognition.
 * @param XHIE                Enable interrupt generation on X high event
 *                            or on direction recognition.
 * @param XLIE                Enable interrupt generation on X low event
 *                            or on direction recognition.
 *
 * @sa LIS_ANDORInterrupt
 */
void LIS3DH_WriteInt1Cfg(uint8_t AndOrInterrupt, uint8_t Enable6D, uint8_t ZHIE,
    uint8_t ZLIE, uint8_t YHIE, uint8_t YLIE, uint8_t XHIE, uint8_t XLIE)
{
  SPISendByte(LIS3DH_REG_INT1_CFG, (AndOrInterrupt << 7) | (Enable6D << 6) | (ZHIE << 5) | (ZLIE << 4) |
      (YHIE << 3) | (YLIE << 2) | (XHIE << 1) | XLIE);
}

/**
 * @brief Read out which source triggered INT1.
 *
 * @return bit6: IntActive     Indicates if an interrupt has been generated or not.
 *         bit5: ZHigh         Z-high event has occurred.
 *         bit4: ZLow          Z-low event has occurred.
 *         bit3: YHigh         Y-high event has occurred.
 *         bit2: YLow          Y-low event has occurred.
 *         bit1: XHigh         X-high event has occurred.
 *         bit0: XLow          X-low event has occurred.
 */
uint8_t LIS3DH_ReadInt1Src()
{
  return SPIReadByte(LIS3DH_REG_INT1_SRC);
}

/**
 * @brief Set the threshold for interrupt 1.
 *
 * @param Threshold     The threshold is a 7bit number.
 *                      1LSB = 16mg @ FS = +-2g
 *                      1LSB = 32mg @ FS = +-4g
 *                      1LSB = 62mg @ FS = +-8g
 *                      1LSB = 186mg @ FS = +-16g
 */
void LIS3DH_WriteInt1Threshold(uint8_t Threshold)
{
  SPISendByte(LIS3DH_REG_INT1_THS, Threshold);
}

/**
 * @brief Set the minimum duration for the interrupt 1 event to be recognized.
 *
 * @param Duration      7bit number for the minimum duration. Duration time is
 *                      measured in N/ODR with N the content of the duration
 *                      register and ODR the selected data rate.
 */
void LIS3DH_WriteInt1Duration(uint8_t Duration)
{
  SPISendByte(LIS3DH_REG_INT1_DURATION, Duration);
}

/** @}*/
