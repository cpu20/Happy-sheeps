/**
* Copyright (c) 2018 Tijl Schepens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*/

#include "em_device.h"
#include <stdint.h>
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_i2c.h"
#include "em_gpio.h"
#include "em_rtc.h"
#include "bsp.h"
#include "LIS3DH.h"
#include "TCS34725.h"
#include "gfx.h"
#include "stdio.h"

/* Local defines. */
#define ON_BOARD_BUTTON0_PORT      gpioPortC
#define ON_BOARD_BUTTON0_PIN       9
#define ON_BOARD_BUTTON1_PORT      gpioPortC
#define ON_BOARD_BUTTON1_PIN       10

/* Global variables. */
/* Keeps track of the current state.
* 0: Track data and put values on the display
* 1: Go into deep sleep and wait for an interrupt from the LIS3DH
*/
uint8_t state = 0;
font_t font;
coord_t fheight;

/***************************************************************************//**
 * @brief  Main function
 ******************************************************************************/
int main(void)
{
  int count = 0;
  uint16_t clearData, redData, greenData, blueData;
  int16_t xData, yData, zData;
  uint8_t first = 1;

  gfxInit();

  /* Get a font. */
  font = gdispOpenFont("*");
  fheight = gdispGetFontMetric(font, fontHeight)+2;

  /* Configure the on-board buttons. */
  GPIO_PinModeSet(ON_BOARD_BUTTON0_PORT, ON_BOARD_BUTTON0_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(ON_BOARD_BUTTON1_PORT, ON_BOARD_BUTTON1_PIN, gpioModeInput, 0);
  /* Configure the pin to generate an interrupt on a rising edge. */
  GPIO_ExtIntConfig(ON_BOARD_BUTTON0_PORT, ON_BOARD_BUTTON0_PIN, ON_BOARD_BUTTON0_PIN,
      true, false, true);
  GPIO_ExtIntConfig(ON_BOARD_BUTTON1_PORT, ON_BOARD_BUTTON1_PIN, ON_BOARD_BUTTON1_PIN,
        true, false, true);
  /* Clear out any possible pending interrupts. */
  GPIO_IntClear(1<<ON_BOARD_BUTTON0_PIN);
  GPIO_IntClear(1<<ON_BOARD_BUTTON1_PIN);
  /* Let the NVIC enable the interrupts. */
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);

  /* Initialize the RGB-sensor and the accelerometer. */
  InitTCS();
  InitLIS3DH();
  /* Enable tilt detection on the accelerometer. */
  LIS3DH_Enable_Detection4D();

  /* Initialize LED driver */
  BSP_LedsInit();

  /* Infinite blink loop */
  while (1) {
    if(!state){
      /* Only print the default text once. */
      if(first){
        /* Clear the screen. */
        gdispClear(White);
        /* Print the text for the RGB-sensor. */
        gdispFillStringBox(10, 10, 42, fheight, "Clear:", font, Black, White, justifyLeft);
        gdispFillStringBox(10, 20, 42, fheight, "Red:", font, Black, White, justifyLeft);
        gdispFillStringBox(10, 30, 42, fheight, "Green:", font, Black, White, justifyLeft);
        gdispFillStringBox(10, 40, 42, fheight, "Blue:", font, Black, White, justifyLeft);
        /* Print the text for the accelerometer. */
        gdispFillStringBox(10, 60, 15, fheight, "X:", font, Black, White, justifyLeft);
        gdispFillStringBox(10, 70, 15, fheight, "Y:", font, Black, White, justifyLeft);
        gdispFillStringBox(10, 80, 15, fheight, "Z:", font, Black, White, justifyLeft);
        /* Text was written. */
        first = 0;
      }
      gfxSleepMilliseconds(250);
      /* Is valid data available? */
      if(TCS_Read_Status() & 0x1){
        /* Get all the data. */
        clearData = TCS_Read_ClearData();
        redData = TCS_Read_RedData();
        greenData = TCS_Read_GreenData();
        blueData = TCS_Read_BlueData();
        /* Convert the uint16_t to strings. */
        char clearD[5], redD[5], greenD[5], blueD[5];
        sprintf(clearD, "%d", clearData);
        sprintf(redD, "%d", redData);
        sprintf(greenD, "%d", greenData);
        sprintf(blueD, "%d", blueData);
        /*  Print the raw data on the screen. */
        gdispFillStringBox(45, 10, 40, fheight, clearD, font, Black, White, justifyCenter);
        gdispFillStringBox(45, 20, 40, fheight, redD, font, Black, White, justifyCenter);
        gdispFillStringBox(45, 30, 40, fheight, greenD, font, Black, White, justifyCenter);
        gdispFillStringBox(45, 40, 40, fheight, blueD, font, Black, White, justifyCenter);
        /* Print out the calculated RGB values. */
        redData = (uint16_t)(((float)redData/(float)clearData)*256);
        sprintf(redD, "%d", redData);
        gdispFillStringBox(88, 20, 40, fheight, redD, font, Black, White, justifyCenter);
        greenData = (uint16_t)(((float)greenData/(float)clearData)*256);
        sprintf(greenD, "%d", greenData);
        gdispFillStringBox(88, 30, 40, fheight, greenD, font, Black, White, justifyCenter);
        blueData = (uint16_t)(((float)blueData/(float)clearData)*256);
        sprintf(blueD, "%d", blueData);
        gdispFillStringBox(88, 40, 40, fheight, blueD, font, Black, White, justifyCenter);
      }
      if(LIS3DH_ReadStatus() & 0x8){
        /* Get all the data (In low power-mode the output resolution is 8-bits).
         * Data is left-justified so we need to shift the data from the MSBs to
         * the LSBs
         */
        xData = LIS3DH_ReadX()/256;
        yData = LIS3DH_ReadY()/256;
        zData = LIS3DH_ReadZ()/256;
        /* Convert the data to strings. */
        char xStr[8], yStr[8], zStr[8];
        sprintf(xStr, "%d", xData);
        sprintf(yStr, "%d", yData);
        sprintf(zStr, "%d", zData);
        /*  Print the data on the screen. */
        gdispFillStringBox(28, 60, 40, fheight, xStr, font, Black, White, justifyCenter);
        gdispFillStringBox(28, 70, 40, fheight, yStr, font, Black, White, justifyCenter);
        gdispFillStringBox(28, 80, 40, fheight, zStr, font, Black, White, justifyCenter);
        /* Convert the data to g-forces. g = (Data/(62.5)) * 9,81m/s² */
        float xg = (((float)xData/(float)62.5))*9.81;
        float yg = (((float)yData/(float)62.5))*9.81;
        float zg = (((float)zData/(float)62.5))*9.81;
        sprintf(xStr, "%d.%d", (int)xg, xg<0 ? (int)(-(xg-(int)xg)*100) : (int)((xg-(int)xg)*100));
        sprintf(yStr, "%d.%d", (int)yg, yg<0 ? (int)(-(yg-(int)xg)*100) : (int)((yg-(int)yg)*100));
        sprintf(zStr, "%d.%d", (int)zg, zg<0 ? (int)(-(zg-(int)xg)*100) : (int)((zg-(int)zg)*100));
        /*  Print the data on the screen. */
        gdispFillStringBox(75, 60, 50, fheight, xStr, font, Black, White, justifyCenter);
        gdispFillStringBox(75, 70, 50, fheight, yStr, font, Black, White, justifyCenter);
        gdispFillStringBox(75, 80, 50, fheight, zStr, font, Black, White, justifyCenter);
      }
      count++;
      BSP_LedsSet(count);
    }
    else{
      /* Currently the sensor data text is on the screen.
       * Clear it and write new text.
       */
      if(!first){
        gdispClear(White);
        gdispFillStringBox(10, 64, 108, fheight, "Entering sleep", font, Black, White, justifyCenter);
        first = 1;
      }
      /* Disable LEDs. */
      BSP_LedsSet(0);
      /* Disable the RGB sensor. */
      TCS_Disable();
      /* Disable the RTC. This will disable the display. */
      RTC_Enable(false);
      SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
      /* Go into deep sleep and wait for accelerometer or button interrupt. */
      EMU_EnterEM3(true);
      SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
      /* Enable the display again. */
      RTC_Enable(true);
      /* Enable the RGB sensor. */
      TCS_Enable();
    }
  }
}

void GPIO_EVEN_IRQHandler(void)
{
  if(GPIO_PinInGet(LIS3DH_INT_PORT, LIS3DH_INT_PIN)){
    GPIO_IntClear(1<<LIS3DH_INT_PIN);
    gdispFillStringBox(10, 70+(fheight*2), 108, fheight, "INT1 occurred", font, Black, White, justifyCenter);
    LIS3DH_ReadInt1Src();
  }
  else if(GPIO_PinInGet(ON_BOARD_BUTTON1_PORT, ON_BOARD_BUTTON1_PIN)){
    GPIO_IntClear(1<<ON_BOARD_BUTTON1_PIN);
    gdispFillStringBox(10, 70+(fheight*2), 108, fheight, "", font, Black, White, justifyCenter);
  }
}

void GPIO_ODD_IRQHandler(void)
{
  if(GPIO_PinInGet(ON_BOARD_BUTTON0_PORT, ON_BOARD_BUTTON0_PIN)){
    GPIO_IntClear(1<<ON_BOARD_BUTTON0_PIN);
    state ^= 1;
  }
}

