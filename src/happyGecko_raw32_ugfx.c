#include "../../../gfx.h"
#include "em_cmu.h"
#include "em_chip.h"
#undef Red
#undef Green
#undef Blue

static volatile uint32_t msTicks; /* counts 1ms timeTicks */

/***************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 ******************************************************************************/
void SysTick_Handler(void)
{
  msTicks++;       /* Increment counter. */
}

systemticks_t gfxSystemTicks(void)
{
    return msTicks;
}

systemticks_t gfxMillisecondsToTicks(delaytime_t ms)
{
    return ms;
}

void Raw32OSInit(void) {
  /* Chip errata */
  CHIP_Init();

  /* Setup SysTick Timer for 1 msec interrupts  */
  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) {
    while (1);
  }
}
