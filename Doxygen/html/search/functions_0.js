var searchData=
[
  ['i2cinit',['I2CInit',['../group__TCS34725.html#ga7d23473bd5eb72909c1a3588a70bb237',1,'TCS34725.c']]],
  ['i2creadbyte',['I2CReadByte',['../group__TCS34725.html#gab59ca5db6b00cb4707ee30e4f903aedc',1,'TCS34725.c']]],
  ['i2creaddata',['I2CReadData',['../group__TCS34725.html#ga17b0c5a8b3fb24c4d5a60028167f713b',1,'TCS34725.c']]],
  ['i2csendbyte',['I2CSendByte',['../group__TCS34725.html#ga8e4a4586488762f5b9cdfb44cc61753c',1,'TCS34725.c']]],
  ['i2csenddata',['I2CSendData',['../group__TCS34725.html#gae46cd8bc113ca7e9f60e26d18908bd95',1,'TCS34725.c']]],
  ['initlis3dh',['InitLIS3DH',['../group__LIS3DH.html#gad79741ae3c0eaa027021f1a7e115b9f0',1,'LIS3DH.c']]],
  ['inittcs',['InitTCS',['../group__TCS34725.html#ga4edaab1474022a84bed76dd21933ddc1',1,'TCS34725.c']]]
];
