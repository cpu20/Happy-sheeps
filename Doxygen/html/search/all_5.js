var searchData=
[
  ['tcs34725',['TCS34725',['../group__TCS34725.html',1,'']]],
  ['tcs34725_2ec',['TCS34725.c',['../TCS34725_8c.html',1,'']]],
  ['tcs34725_2eh',['TCS34725.h',['../TCS34725_8h.html',1,'']]],
  ['tcs34725_5faddr',['TCS34725_ADDR',['../group__TCS34725.html#ga14c351e2c44915b8809d3ac688e86374',1,'TCS34725.h']]],
  ['tcs34725_5fauto_5finc',['TCS34725_AUTO_INC',['../group__TCS34725.html#ga2cd81d2771e39bdeb2c1725b2ffd0ac9',1,'TCS34725.h']]],
  ['tcs34725_5fcmd',['TCS34725_CMD',['../group__TCS34725.html#ga8b5eb94f7f656804023d4c411be19075',1,'TCS34725.h']]],
  ['tcs34725_5freg_5faihth',['TCS34725_REG_AIHTH',['../group__TCS34725.html#gac01a0008460183201d2ac4a7ecb43ff0',1,'TCS34725.h']]],
  ['tcs34725_5freg_5faihtl',['TCS34725_REG_AIHTL',['../group__TCS34725.html#ga3b618511e6d76ebd6c392f50780ca3e0',1,'TCS34725.h']]],
  ['tcs34725_5freg_5failth',['TCS34725_REG_AILTH',['../group__TCS34725.html#ga8df91cd405d2d2bdcd3eb25ef3e94ab0',1,'TCS34725.h']]],
  ['tcs34725_5freg_5failtl',['TCS34725_REG_AILTL',['../group__TCS34725.html#gac1fec8d109b3811b7b3df9d3ccd65556',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fatime',['TCS34725_REG_ATIME',['../group__TCS34725.html#ga81820604cdc4379f755e323afb238d31',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fbdatah',['TCS34725_REG_BDATAH',['../group__TCS34725.html#gac3f5eee2e25302bea345bf00fb1ec153',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fbdatal',['TCS34725_REG_BDATAL',['../group__TCS34725.html#gadf320c6ec38e070f7d942b1813df3991',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fcdatal',['TCS34725_REG_CDATAL',['../group__TCS34725.html#ga1dfa1f0baebee0049b282431d1f53b77',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fconfig',['TCS34725_REG_CONFIG',['../group__TCS34725.html#ga0c706735818a1053eae5b81ebf3f0102',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fcontrol',['TCS34725_REG_CONTROL',['../group__TCS34725.html#ga759179ba0caeec927c074634e2d42554',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fenable',['TCS34725_REG_ENABLE',['../group__TCS34725.html#ga0e84c7911cad0029ef4cac2faaab063d',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fgdatah',['TCS34725_REG_GDATAH',['../group__TCS34725.html#ga2f21ed3d3230e9b8097b8ccac3b2e1a1',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fgdatal',['TCS34725_REG_GDATAL',['../group__TCS34725.html#gaafcb470770b8226de5d93db53282c743',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fid',['TCS34725_REG_ID',['../group__TCS34725.html#ga0a836cec3a7e90b3007d17b9162d8ab5',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fpers',['TCS34725_REG_PERS',['../group__TCS34725.html#ga711c12f07a377fc4094961c7ffe6d60b',1,'TCS34725.h']]],
  ['tcs34725_5freg_5frdatah',['TCS34725_REG_RDATAH',['../group__TCS34725.html#ga8053dc0dc4ed3d2ba677e496be2ee5cd',1,'TCS34725.h']]],
  ['tcs34725_5freg_5frdatal',['TCS34725_REG_RDATAL',['../group__TCS34725.html#ga3f7120f7351b84b5329873c36149f581',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fstatus',['TCS34725_REG_STATUS',['../group__TCS34725.html#ga1af428b049175e02cfe340186d2a6672',1,'TCS34725.h']]],
  ['tcs34725_5freg_5fwtime',['TCS34725_REG_WTIME',['../group__TCS34725.html#ga39cbb2bd3d848bfb6978453fcc09f540',1,'TCS34725.h']]],
  ['tcs43725_5freg_5fcdatah',['TCS43725_REG_CDATAH',['../group__TCS34725.html#ga02e4b6e5c0293bf70f4c401a859b4bc0',1,'TCS34725.h']]],
  ['tcs_5fdisable',['TCS_Disable',['../group__TCS34725.html#ga0adbd071937292dabc6f1cae03c126dd',1,'TCS34725.c']]],
  ['tcs_5fenable',['TCS_Enable',['../group__TCS34725.html#ga867409d1d342ce32ff4db6109d4ef9de',1,'TCS34725.c']]],
  ['tcs_5fenabledisable',['TCS_EnableDisable',['../group__TCS34725.html#ga1e4df6ad5786d57439ea4f74a5641685',1,'TCS34725.h']]],
  ['tcs_5fget_5fwaittime',['TCS_Get_WaitTime',['../group__TCS34725.html#ga95cb11e6d5c7c9d19124d5b36bf28845',1,'TCS34725.c']]],
  ['tcs_5finitstatus',['TCS_InitStatus',['../group__TCS34725.html#ga216ac3bfea6a5d53d3aac01aec5cf147',1,'TCS34725.h']]],
  ['tcs_5fread_5fbluedata',['TCS_Read_BlueData',['../group__TCS34725.html#gae3b162ed7b27bee2a6999a280e7ec19f',1,'TCS34725.c']]],
  ['tcs_5fread_5fcleardata',['TCS_Read_ClearData',['../group__TCS34725.html#ga1c75a5bca0c38bf07a959179f5931a73',1,'TCS34725.c']]],
  ['tcs_5fread_5fenablereg',['TCS_Read_EnableReg',['../group__TCS34725.html#gaf68767fde4caefcd0792f595861f5a0f',1,'TCS34725.c']]],
  ['tcs_5fread_5fgreendata',['TCS_Read_GreenData',['../group__TCS34725.html#gae577f2b45b9a6b6949597bdaba8b8c85',1,'TCS34725.c']]],
  ['tcs_5fread_5fintegrationtime',['TCS_Read_IntegrationTime',['../group__TCS34725.html#gab1a3b69255750c27157842d7e433b194',1,'TCS34725.c']]],
  ['tcs_5fread_5freddata',['TCS_Read_RedData',['../group__TCS34725.html#gabbf1d9caa1d26b8ba54686b3446f5121',1,'TCS34725.c']]],
  ['tcs_5fread_5fstatus',['TCS_Read_Status',['../group__TCS34725.html#gab21a09cc8662bf5ab942c4122e8ed599',1,'TCS34725.c']]],
  ['tcs_5fset_5fintegrationtime',['TCS_Set_IntegrationTime',['../group__TCS34725.html#gafc84be0192ff619ede22b2944943cb9f',1,'TCS34725.c']]],
  ['tcs_5fset_5fwaittime',['TCS_Set_WaitTime',['../group__TCS34725.html#ga7ba7d8b8f777469aabc4b1f975145e27',1,'TCS34725.c']]],
  ['tcs_5fwrite_5fconfigreg',['TCS_Write_ConfigReg',['../group__TCS34725.html#gaef04c06637b0f7e5ff4adb6f13fb5f23',1,'TCS34725.c']]],
  ['tcs_5fwrite_5fcontrolreg',['TCS_Write_ControlReg',['../group__TCS34725.html#ga1b3cc381bd0b427b54e259e3cd688be3',1,'TCS34725.c']]],
  ['tcs_5fwrite_5fenablereg',['TCS_Write_EnableReg',['../group__TCS34725.html#gab1d139a79c9bfa36728484d1b5c77368',1,'TCS34725.c']]],
  ['tcsinitfailed',['TCSInitFailed',['../group__TCS34725.html#gga216ac3bfea6a5d53d3aac01aec5cf147aeff99bf44e405ea3f204c719a807c961',1,'TCS34725.h']]],
  ['tcsinitsuccess',['TCSInitSuccess',['../group__TCS34725.html#gga216ac3bfea6a5d53d3aac01aec5cf147a49ee30aae3e0bb5aaea9fe8c4c9e7e9e',1,'TCS34725.h']]]
];
