var searchData=
[
  ['spiinit',['SPIInit',['../group__LIS3DH.html#gaa738ada19c5774090d23f39b5beecb2b',1,'LIS3DH.c']]],
  ['spireadbyte',['SPIReadByte',['../group__LIS3DH.html#gac832b8fcf29ce75e9aa64bdcda0619bf',1,'LIS3DH.c']]],
  ['spireaddata',['SPIReadData',['../group__LIS3DH.html#gaadf670a081f62262c456d2867aaa0133',1,'LIS3DH.c']]],
  ['spisendbyte',['SPISendByte',['../group__LIS3DH.html#gabb89ff28140baff3ac2202f1c8a0407f',1,'LIS3DH.c']]],
  ['spisenddata',['SPISendData',['../group__LIS3DH.html#ga5046352ff59f2f873a67b5fbdaeb7954',1,'LIS3DH.c']]],
  ['status_5ferror',['Status_Error',['../group__TCS34725.html#ggab99ccd75ef2ee3524bc7e492cc58521faf1f4bd2cf25652d7920f9e9e2f2c12ee',1,'TCS34725.h']]],
  ['status_5fok',['Status_OK',['../group__TCS34725.html#ggab99ccd75ef2ee3524bc7e492cc58521fae3050bc378546ad9246f8c81f7893297',1,'TCS34725.h']]],
  ['status_5ftimeout',['Status_Timeout',['../group__TCS34725.html#ggab99ccd75ef2ee3524bc7e492cc58521fa25372a58a2850792a49ff8543ff9aeb0',1,'TCS34725.h']]]
];
