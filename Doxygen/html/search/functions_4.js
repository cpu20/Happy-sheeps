var searchData=
[
  ['tcs_5fdisable',['TCS_Disable',['../group__TCS34725.html#ga0adbd071937292dabc6f1cae03c126dd',1,'TCS34725.c']]],
  ['tcs_5fenable',['TCS_Enable',['../group__TCS34725.html#ga867409d1d342ce32ff4db6109d4ef9de',1,'TCS34725.c']]],
  ['tcs_5fget_5fwaittime',['TCS_Get_WaitTime',['../group__TCS34725.html#ga95cb11e6d5c7c9d19124d5b36bf28845',1,'TCS34725.c']]],
  ['tcs_5fread_5fbluedata',['TCS_Read_BlueData',['../group__TCS34725.html#gae3b162ed7b27bee2a6999a280e7ec19f',1,'TCS34725.c']]],
  ['tcs_5fread_5fcleardata',['TCS_Read_ClearData',['../group__TCS34725.html#ga1c75a5bca0c38bf07a959179f5931a73',1,'TCS34725.c']]],
  ['tcs_5fread_5fenablereg',['TCS_Read_EnableReg',['../group__TCS34725.html#gaf68767fde4caefcd0792f595861f5a0f',1,'TCS34725.c']]],
  ['tcs_5fread_5fgreendata',['TCS_Read_GreenData',['../group__TCS34725.html#gae577f2b45b9a6b6949597bdaba8b8c85',1,'TCS34725.c']]],
  ['tcs_5fread_5fintegrationtime',['TCS_Read_IntegrationTime',['../group__TCS34725.html#gab1a3b69255750c27157842d7e433b194',1,'TCS34725.c']]],
  ['tcs_5fread_5freddata',['TCS_Read_RedData',['../group__TCS34725.html#gabbf1d9caa1d26b8ba54686b3446f5121',1,'TCS34725.c']]],
  ['tcs_5fread_5fstatus',['TCS_Read_Status',['../group__TCS34725.html#gab21a09cc8662bf5ab942c4122e8ed599',1,'TCS34725.c']]],
  ['tcs_5fset_5fintegrationtime',['TCS_Set_IntegrationTime',['../group__TCS34725.html#gafc84be0192ff619ede22b2944943cb9f',1,'TCS34725.c']]],
  ['tcs_5fset_5fwaittime',['TCS_Set_WaitTime',['../group__TCS34725.html#ga7ba7d8b8f777469aabc4b1f975145e27',1,'TCS34725.c']]],
  ['tcs_5fwrite_5fconfigreg',['TCS_Write_ConfigReg',['../group__TCS34725.html#gaef04c06637b0f7e5ff4adb6f13fb5f23',1,'TCS34725.c']]],
  ['tcs_5fwrite_5fcontrolreg',['TCS_Write_ControlReg',['../group__TCS34725.html#ga1b3cc381bd0b427b54e259e3cd688be3',1,'TCS34725.c']]],
  ['tcs_5fwrite_5fenablereg',['TCS_Write_EnableReg',['../group__TCS34725.html#gab1d139a79c9bfa36728484d1b5c77368',1,'TCS34725.c']]]
];
