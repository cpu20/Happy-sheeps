var indexSectionsWithContent =
{
  0: "ilmrst",
  1: "lmt",
  2: "ilmst",
  3: "ilt",
  4: "lst",
  5: "lt"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "enums",
  4: "enumvalues",
  5: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Enumerations",
  4: "Enumerator",
  5: "Modules"
};

