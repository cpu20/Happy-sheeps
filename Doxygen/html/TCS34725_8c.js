var TCS34725_8c =
[
    [ "I2CInit", "group__TCS34725.html#ga7d23473bd5eb72909c1a3588a70bb237", null ],
    [ "I2CReadByte", "group__TCS34725.html#gab59ca5db6b00cb4707ee30e4f903aedc", null ],
    [ "I2CReadData", "group__TCS34725.html#ga17b0c5a8b3fb24c4d5a60028167f713b", null ],
    [ "I2CSendByte", "group__TCS34725.html#ga8e4a4586488762f5b9cdfb44cc61753c", null ],
    [ "I2CSendData", "group__TCS34725.html#gae46cd8bc113ca7e9f60e26d18908bd95", null ],
    [ "InitTCS", "group__TCS34725.html#ga4edaab1474022a84bed76dd21933ddc1", null ],
    [ "TCS_Disable", "group__TCS34725.html#ga0adbd071937292dabc6f1cae03c126dd", null ],
    [ "TCS_Enable", "group__TCS34725.html#ga867409d1d342ce32ff4db6109d4ef9de", null ],
    [ "TCS_Get_WaitTime", "group__TCS34725.html#ga95cb11e6d5c7c9d19124d5b36bf28845", null ],
    [ "TCS_Read_BlueData", "group__TCS34725.html#gae3b162ed7b27bee2a6999a280e7ec19f", null ],
    [ "TCS_Read_ClearData", "group__TCS34725.html#ga1c75a5bca0c38bf07a959179f5931a73", null ],
    [ "TCS_Read_EnableReg", "group__TCS34725.html#gaf68767fde4caefcd0792f595861f5a0f", null ],
    [ "TCS_Read_GreenData", "group__TCS34725.html#gae577f2b45b9a6b6949597bdaba8b8c85", null ],
    [ "TCS_Read_IntegrationTime", "group__TCS34725.html#gab1a3b69255750c27157842d7e433b194", null ],
    [ "TCS_Read_RedData", "group__TCS34725.html#gabbf1d9caa1d26b8ba54686b3446f5121", null ],
    [ "TCS_Read_Status", "group__TCS34725.html#gab21a09cc8662bf5ab942c4122e8ed599", null ],
    [ "TCS_Set_IntegrationTime", "group__TCS34725.html#gafc84be0192ff619ede22b2944943cb9f", null ],
    [ "TCS_Set_WaitTime", "group__TCS34725.html#ga7ba7d8b8f777469aabc4b1f975145e27", null ],
    [ "TCS_Write_ConfigReg", "group__TCS34725.html#gaef04c06637b0f7e5ff4adb6f13fb5f23", null ],
    [ "TCS_Write_ControlReg", "group__TCS34725.html#ga1b3cc381bd0b427b54e259e3cd688be3", null ],
    [ "TCS_Write_EnableReg", "group__TCS34725.html#gab1d139a79c9bfa36728484d1b5c77368", null ]
];